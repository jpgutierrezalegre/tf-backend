require("dotenv").config();

const request_json = require("request-json");

const MLAB_DBNAME = "" || process.env.MLAB_DBNAME;
const MLAB_APIKEY = "apiKey=" + process.env.MLAB_APIKEY;
const MLAB_URL =
  "https://api.mlab.com/api/1/databases/" + MLAB_DBNAME + "/collections/";

module.exports = {
  getEntity: (entity, options) => {
    return new Promise((resolve, reject) => {
      let optionsString = options || "";

      const http_client = request_json.createClient(MLAB_URL);

      http_client.get(
        entity + "?" + optionsString + "&" + MLAB_APIKEY,
        (err, res, body) => {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        }
      );
    });
  },
  postEntity: (entity, entityBody) => {
    return new Promise((resolve, reject) => {
      const http_client = request_json.createClient(MLAB_URL);

      http_client.post(
        entity + "?" + MLAB_APIKEY,
        entityBody,
        (err, res, body) => {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        }
      );
    });
  },
  putEntity: (entity, dbId, entityBody) => {
    return new Promise((resolve, reject) => {
      const httpClient = request_json.createClient(MLAB_URL);
      httpClient.put(
        entity + "/" + dbId + "?" + MLAB_APIKEY,
        entityBody,
        function (err, res, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        }
      );
    });
  },
};
