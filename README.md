# Backend del Proyecto Final - Practitioner 2020

API REST desarrollado por Juan Pablo Gutiérrez Alegre como Proyecto Final - TechU Practitioner 2020.
Ofrece la posibilidad de gestionar usuarios, cuentas y movimientos. Utiliza JWT como mecanismo de seguridad. El JWT se obtiene a través del servicio login.
El proyecto está construido con JavaScript sobre NodeJS. Como persistencia utiliza MongoLab (mLab), con el que la comunicación se realiza a través de [mLab Data API](https://docs.mlab.com/data-api/).

## Pasos para disponibilizar proyecto

Instalar dependencias:
```sh
npm install
```
Crear archivo `.env` con la siguiente estructura:
```sh
PORT=<puerto de salida del API>
MLAB_APIKEY=<MLAB API KEY>
MLAB_DBNAME=<base de datos MLAB>
JWT_SECRET=<password a usar para generar Token JWT (opcional)>
JWT_ALGORITHM=<algoritmo criptográfico a usar para generar Token JWT (opcional)>
```
Para entorno de desarrollo, bastará con ejecutar el siguiente comando. Al aplicar un cambio, el servidor se reiniciará automáticamente:
```sh
npm run dev
```
Para entorno productivo, el comando a ejecutar es el siguiente:
```sh
npm run prod
```

### Integración con Docker
El proyecto incluye un archivo `Dockerfile` que permite levantar el proyecto como contenedor.
Primero, se debe construir la imagen a partir del archivo `Dockerfile`. En la carpeta del proyecto, ejectuar el siguiente comando: 
```sh
docker build --tag <tag_name>:<version> .
```
Ejemplo:
```sh 
docker build --tag techu:1.0 .
```
Luego, se podrá ejecutar el contenedor construido:
```sh
docker run --p <puerto_externo>:<puerto_interno_env> <tag_name>:<version>
```
Ejemplo:
```sh
docker run --p 8080:3000 techu:1.0
```

### Configuración Inicial de Base de Datos
Como se mencionó previamente, el proyecto utiliza mLab como Base de datos. Es necesario crear las colecciones ***user***, ***movement*** y ***account***.

Al levantar el proyecto, se mostrará en la consola el siguiente mensaje:
```sh
Backend server up! Listening on: 3000
Default 'admin' encryted password:
$2b$04$19K9TbbMwKIdHevTphJMV.dut3cZkgACT9Z0w5/gXMtBczlOWVwFi
```
Esa cadena encriptada corresponde a la contraseña *"admin"*. Será necesaria para crear el usuario administrador por defecto.

Insertar el siguiente documento en la colección ***user***:
```sh
{
    "id": 1,
    "documentNumber": "<usuario_administrador>",
    "firstName": "Admin",
    "lastName": "Admin",
    "password": "<cadena encriptada correspondiente a "admin">",
    "isAdmin": true
}
```

Ejemplo: 
```sh
{
    "id": 1,
    "documentNumber": "admin",
    "firstName": "Admin",
    "lastName": "Admin",
    "password": "$2b$04$19K9TbbMwKIdHevTphJMV.dut3cZkgACT9Z0w5/gXMtBczlOWVwFi",
    "isAdmin": true
}
```
Luego se podrá invocar el servicio ***/login*** para obtener el JWT correspondiente.

## Servicios desarrollados

### Autenticación:
- **/login - POST**
    Permite realizar autenticación de usuario y obtener token JWT, necesario para el resto de servicios.

### Usuarios:
- **/users - GET**
    Permite obtener listado de usuarios.

- **/users/:id - GET**
    Permite obtener el detalle de un usuario por ID.

- **/users - POST**
    Permite crear un nuevo usuario

- **/users/:id - PUT**
    Permite actualizar un usuario

### Cuentas:
- **/accounts - GET**
    Permite obtener listado de cuentas del usuario

- **/accounts/:accountNumber - GET**
    Permite obtener el detalle de una cuenta por número de cuenta.

- **/accounts/:accountNumber/owner - GET**
    Permite obtener el detalle del propietario de una cuenta por número de cuenta. 

- **/accounts - POST**
    Permite crear una nueva cuenta.

- **/accounts/:accountNumber - DELETE**
    Permite marcar como inactiva una cuenta por número de cuenta.

- **/accounts/:accountNumber/movements - POST**
    Permite crear un movimiento tomando como origen la cuenta indicada.

- **/accounts/:accountNumber - PATCH**
    Permite registar una operación sobre una cuenta y actualizar el saldo.