require("dotenv").config();
const express = require("express");
const body_parser = require("body-parser");
const app = express();
const cors = require("cors");
const jwt = require("jsonwebtoken");
const express_jwt = require("express-jwt");

const bcrypt = require('bcrypt');
const CRYPTO_SALT_ROUNDS = 1;

const mlab_utils = require("./utils/mlab");
const data_utils = require("./utils/data");

const JWT_SECRET = process.env.JWT_SECRET || "defaultJwtKey";
const JWT_ALGORITHM = process.env.JWT_ALGORITHM || "HS256";

let API_PORT = process.env.PORT || 3000;
const API_BASE_URL = "/techu/v1/";

const ERROR = {
  GENERAL: "Error interno.",
  TOKEN_INVALIDO: "Token incorrecto.",
  LOGIN_INCORRECTO: "Login incorrecto.",
  USUARIO_NO_ENCONTRADO: "Usuario no encontrado",
  DOCUMENT_INVALIDO: "Número de documento inválido",
};

app.listen(API_PORT, function () {
  console.log("Backend server up! Listening on: " + API_PORT);
  
  const hashed = bcrypt.hashSync('admin', CRYPTO_SALT_ROUNDS);
  console.log("Default 'admin' encryted password: ");
  console.log(hashed);

});

app.use(body_parser.json());

app.use(cors());
app.options("*", cors());

app.use(
  express_jwt({
    secret: JWT_SECRET,
    requestProperty: "auth",
    algorithms: [JWT_ALGORITHM],
  }).unless({
    path: [API_BASE_URL + "login"],
  })
);

app.use(function (err, req, res, next) {
  if (err.name === "UnauthorizedError") {
    res.status(401).send({ msg: ERROR.TOKEN_INVALIDO });
  }
});

const onlyAdminFilter = function (req, res, next) {
  if (req.auth.isAdmin) {
	next();
  } else {
	res.status(401).send({ msg: ERROR.TOKEN_INVALIDO });  
  }
}

const onlyNonAdminFilter = function (req, res, next) {
  if (!req.auth.isAdmin) {
	next();
  } else {
	res.status(401).send({ msg: ERROR.TOKEN_INVALIDO });  
  }
}

// login
app.post(API_BASE_URL + "login", (request, response) => {
  const findUserByDocumentNumber = mlab_utils.getEntity(
    "user",
    'q={"documentNumber":"' + request.body.documentNumber + '"}'
  );

  findUserByDocumentNumber
    .then((res) => {
      if (res.length == 0 || !bcrypt.compareSync(request.body.password, res[0].password)) {
        response.status(400).send({ msg: ERROR.LOGIN_INCORRECTO });
      } else {
        let currentUser = res[0];

        const payload = {
          idUsuario: currentUser.id,
          isAdmin: currentUser.isAdmin
        };

        const token = jwt.sign(payload, JWT_SECRET, {
          algorithm: JWT_ALGORITHM,
          expiresIn: "1d",
        });

        response.send({
          token: token,
          isAdmin: currentUser.isAdmin,
          name: currentUser.firstName + ' ' + (currentUser.middleName || '')
        });
      }
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });
});

// USERS API

// list all
app.get(API_BASE_URL + "users", onlyAdminFilter, (request, response) => {
  mlab_utils
    .getEntity("user", 'f={"_id":0, "password":0}&s={"id": 1}')
    .then((res) => {
      response.status(200).send(res);
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });
});

app.get(API_BASE_URL + "users/:id", onlyAdminFilter, (request, response) => {
  let id = request.params.id;

  let fieldFilter = 'f={"_id":0, "password":0}&';
  let queryString = 'q={"id":' + id + "}";

  mlab_utils.getEntity("user", fieldFilter + queryString).then((res) => {
    let responseBody = {};
    if (res.length > 0) {
      responseBody = res[0];
      response.status(200);
    } else {
      responseBody = { msg: ERROR.USUARIO_NO_ENCONTRADO };
      response.status(404);
    }
    response.send(responseBody);
  });
});

app.post(API_BASE_URL + "users", onlyAdminFilter, (request, response) => {

  const findUserByDocumentNumber = mlab_utils.getEntity(
    "user",
    'q={"documentNumber":"' + request.body.documentNumber + '"}'
  );

  findUserByDocumentNumber.then(resFindUser => {
    if (resFindUser.length > 0) {
      response.status(400).send({ msg: ERROR.DOCUMENT_INVALIDO });
    } else {
      const getNewUserId = mlab_utils.getEntity(
        "user",
        'f={"id":1}&s={"id":-1}&l=1'
      );
    
      getNewUserId
        .then((res) => {
          let newId = res.length > 0 ? res[0].id + 1 : 1;
          let newUser = {
            id: newId,
            documentNumber: request.body.documentNumber,
            firstName: request.body.firstName,
            middleName: request.body.middleName,
            lastName: request.body.lastName,
            secondLastName: request.body.secondLastName,
            email: request.body.email,
            password: bcrypt.hashSync(request.body.password, CRYPTO_SALT_ROUNDS),
            isAdmin: request.body.isAdmin,
          };
          mlab_utils
            .postEntity("user", newUser)
            .then((res) => {
              response
                .status(201)
                .send({ msg: "Usuario creado", id: newUser.id });
            })
            .catch((err) => {
              response.status(500).send({ msg: ERROR.GENERAL });
            });
        })
        .catch((err) => {
          response.status(500).send({ msg: ERROR.GENERAL });
        });
    }
  });

});

app.put(API_BASE_URL + "users/:id", onlyAdminFilter, (request, response) => {
  let id = request.params.id;
  const findUserById = mlab_utils.getEntity("user", 'q={"id":' + id + "}");

  findUserById
    .then((res) => {
      if (res.length == 0) {
        response.status(404).send({ msg: ERROR.USUARIO_NO_ENCONTRADO });
      } else {

        const findUserByDocumentNumber = mlab_utils.getEntity(
          "user",
          'q={"documentNumber":"' + request.body.documentNumber + '", "id": {"$ne": ' + id +'}}'
        );

        findUserByDocumentNumber.then( resFindUserByDocument => {
          if (resFindUserByDocument.length > 0) {
            response.status(400).send({ msg: ERROR.DOCUMENT_INVALIDO });
          } else {
            let currentUser = res[0];
            let updatedUser = {
              id: currentUser.id,
              documentNumber: request.body.documentNumber,
              firstName: request.body.firstName,
              middleName: request.body.middleName,
              lastName: request.body.lastName,
              secondLastName: request.body.secondLastName,
              email: request.body.email,
              password: request.body.password ? bcrypt.hashSync(request.body.password, CRYPTO_SALT_ROUNDS) : currentUser.password,
              isAdmin: currentUser.isAdmin,
            };

            const dbId = currentUser._id.$oid;
            mlab_utils
              .putEntity("user", dbId, updatedUser)
              .then((res) => {
                response
                  .status(202)
                  .send({ msg: "Usuario actualizado", id: updatedUser.id });
              })
              .catch((err) => {
                response.status(500).send({ msg: ERROR.GENERAL });
              });
            }
        });
      }
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });
});

// ACCOUNTS API
app.get(API_BASE_URL + "accounts", (request, response) => {
  const { idUsuario, isAdmin } = request.auth;

  let queryString = "";

  if (isAdmin) {
    if (request.query.userId) {
      queryString = 'q={"userId":' + request.query.userId + '}&f={"_id":0}';
    } else {
      queryString = 'f={"_id":0}';
    }
  } else {
    queryString = 'q={"userId":' + idUsuario + ', "status":1}&f={"_id":0}';
  }

  mlab_utils
    .getEntity("account", queryString)
    .then((res) => {
      response.status(200).send(res);
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });
});

app.get(API_BASE_URL + "accounts/:id", (request, response) => {
  let accountNumber = request.params.id;
  const { idUsuario, isAdmin } = request.auth;

  let accountQueryString = 'f={"_id":0}&';
  if (isAdmin) {
	  accountQueryString += 'q={"accountNumber":"' + accountNumber + '"}';
  } else {
    accountQueryString += 'q={"accountNumber":"' + accountNumber + '", "status": 1, "userId":' + idUsuario + '}';
  }
  
  const checkAccount = mlab_utils.getEntity(
    "account",
    accountQueryString
  );
  
  checkAccount
    .then((res) => {
      if (res.length == 0) {
        response.status(400).send({ msg: "Cuenta no encontrada" });
      } else {
        let currentAccount = res[0];
        const queryString =
          'f={"_id":0}&' +
          'q={"$or":[{"originAccount":"' +
          accountNumber +
          '"}, {"destinationAccount":"' +
          accountNumber +
          '"}]}'+
          '&s={"date":-1}';
        mlab_utils
          .getEntity("movement", queryString)
          .then((res) => {
          currentAccount.movements = res;
          response.status(200).send(currentAccount);
          })
          .catch((err) => {
          response.status(500).send({ msg: ERROR.GENERAL });
          });
          }
        })
        .catch((err) => {
          response.status(500).send({ msg: ERROR.GENERAL });
    });
});

app.get(API_BASE_URL + "accounts/:id/owner", (request, response) => {

  let accountNumber = request.params.id;
  const { idUsuario, isAdmin } = request.auth;

  let accountQueryString = "";
  if (isAdmin) {
	  accountQueryString = 'q={"accountNumber":"' + accountNumber + '"}';
  } else {
    accountQueryString = 'q={"accountNumber":"' + accountNumber + '", "status": 1 }';
  }
  
  const checkAccount = mlab_utils.getEntity(
    "account",
    accountQueryString
  );

  checkAccount
    .then((res) => {
      if (res.length == 0) {
        response.status(400).send({ msg: "Cuenta no encontrada" });
      } else {
        let currentAccount = res[0];
        let fieldFilter = 'f={"_id":0, "password":0}&';
        let queryString = 'q={"id":' + currentAccount.userId + "}";

        mlab_utils.getEntity("user", fieldFilter + queryString).then((res) => {
          let responseBody = {};
          if (res.length > 0) {
            responseBody = res[0];
            response.status(200);
          } else {
            responseBody = { msg: ERROR.USUARIO_NO_ENCONTRADO };
            response.status(404);
          }
          response.send(responseBody);
        });
      }
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });

});

app.post(API_BASE_URL + "accounts", onlyAdminFilter, (request, response) => {
  const { idUsuario, isAdmin } = request.auth;
  const newAccountNumber = request.body.accountNumber;

  const checkAccountNumber = mlab_utils.getEntity(
    "account",
    'q={"accountNumber":"' + newAccountNumber + '"}'
  );

  checkAccountNumber
    .then((res) => {
      if (res.length > 0) {
        response.status(400).send({ msg: "Número de cuenta inválido" });
      } else {
        let newAccount = {
          accountNumber: newAccountNumber,
          userId: request.body.userId,
          currency: request.body.currency,
          balance: 0.0,
          openingDate: {"$date": data_utils.getTodayDate()},
          status: 1,
        };

        mlab_utils
          .postEntity("account", newAccount)
          .then((res) => {
            response
              .status(201)
              .send({ msg: "Cuenta creada", accountNumber: newAccountNumber });
          })
          .catch((err) => {
            response.status(500).send({ msg: ERROR.GENERAL });
          });
      }
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });
});

app.delete(API_BASE_URL + "accounts/:id", onlyAdminFilter, (request, response) => {
  let accountNumber = request.params.id;
  const { idUsuario, isAdmin } = request.auth;
  
  const checkAccount = mlab_utils.getEntity(
    "account",
    'q={"accountNumber":"' + accountNumber + '"}'
  );
  
  checkAccount
    .then((res) => {
      if (res.length == 0) {
        response.status(400).send({ msg: "Cuenta no encontrada" });
      } else {
        let currentAccount = res[0];
		
		if (currentAccount.balance > 0) {
			response.status(400).send({ msg: "Cuenta tiene saldo a favor" });
		} else {
			let updatedAccount = {
			  accountNumber: currentAccount.accountNumber,
			  userId: currentAccount.userId,
			  currency: currentAccount.currency,
			  balance: currentAccount.balance,
			  openingDate: currentAccount.openingDate,
			  status: 0,
			};
			
			mlab_utils.putEntity(
			  "account",
			  currentAccount._id.$oid,
			  updatedAccount
			).then((res) => {
				response.status(200).send({ msg: "Cuenta desactivada", accountNumber: accountNumber });
			  })
			  .catch((err) => {
				 response.status(500).send({ msg: ERROR.GENERAL });
			  });
		}

      }
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });
});

// MOVEMENTS API
app.post(API_BASE_URL + "accounts/:id/movements", onlyNonAdminFilter, (request, response) => {
  const { idUsuario, isAdmin } = request.auth;

  const getAccountByNumber = (accountNumber, ignoreUser) => {
    let accountQueryString = "";
    if (isAdmin || ignoreUser) {
      accountQueryString = 'q={"accountNumber":"' + accountNumber + '", "status": 1}';
    } else {
      accountQueryString = 'q={"accountNumber":"' + accountNumber + '", "status": 1, "userId":' + idUsuario + '}';
    }
	  
    return mlab_utils.getEntity(
      "account",
      accountQueryString
    );
  };

  const updateAccountBalance = (currentAccount, newBalance) => {
    let updatedAccount = {
      accountNumber: currentAccount.accountNumber,
      userId: currentAccount.userId,
      currency: currentAccount.currency,
      balance: newBalance,
      openingDate: currentAccount.openingDate,
      status: currentAccount.status,
    };
    return mlab_utils.putEntity(
      "account",
      currentAccount._id.$oid,
      updatedAccount
    );
  };

  let { destinationAccount, amount, currency, reference } = request.body;
  let originAccount = request.params.id;

  getAccountByNumber(originAccount, false)
    .then((resOriginAccount) => {
      if (resOriginAccount.length == 0) {
        response.status(400).send({ msg: "Cuenta origen no encontrada" });
      } else {
        let originAccountEntity = resOriginAccount[0];
        
		if (originAccountEntity.balance < amount) {
			response.status(400).send({ msg: "Monto inválido" });
		} else {
			getAccountByNumber(destinationAccount, true)
				.then((resDestinationAccount) => {
					if (resDestinationAccount.length == 0) {
					  response
						.status(400)
						.send({ msg: "Cuenta destino no encontrada" });
					} else {
					  let destinationAccountEntity = resDestinationAccount[0];
					  let newMovement = {
              originAccount: originAccount,
              destinationAccount: destinationAccount,
              currency: currency,
              amount: amount,
              date: {"$date": data_utils.getTodayDate()},
              reference: reference,
					  };

					  mlab_utils
						.postEntity("movement", newMovement)
						.then((res) =>
						  updateAccountBalance(
							originAccountEntity,
							originAccountEntity.balance - amount
						  )
						)
						.then((res) =>
						  updateAccountBalance(
							destinationAccountEntity,
							destinationAccountEntity.balance + amount
						  )
						)
						.then((res) => {
						  response.status(201).send({ msg: "Movimiento creado" });
						})
						.catch((err) => {
						  response.status(500).send({ msg: ERROR.GENERAL });
						});
					}
		  })
		  .catch((err) => {
			response.status(500).send({ msg: ERROR.GENERAL });
		  });
		}
        
      }
    })
    .catch((err) => {
      response.status(500).send({ msg: ERROR.GENERAL });
    });
});

app.patch(API_BASE_URL + "accounts/:id", onlyAdminFilter, async (request, response)=> {

  let accountNumber = request.params.id;

  let getAccount = async (account)=> {
    return mlab_utils.getEntity(
      "account",
      'q={"accountNumber":"' + account + '", "status": 1 }'
    );
  }

  try {
    let checkAccountResponse = await getAccount(accountNumber);

    if (checkAccountResponse.length == 0) {
      response.status(400).send({ msg: "Cuenta no encontrada" });
      return;
    }
    let currentAccount = checkAccountResponse[0];
    const amount = request.body.amount;

    if (amount < 0 && currentAccount.balance < (amount * -1)) {
      response.status(400).send({ msg: "Monto inválido" });
      return
    }

    let newMovement = {
      originAccount: amount < 0 ? accountNumber : '',
      destinationAccount: amount > 0 ? accountNumber : '',
      currency: currentAccount.currency,
      amount: amount < 0 ? amount * -1 : amount,
      date: {"$date": data_utils.getTodayDate()},
      reference: amount < 0 ? 'RETIRO' : 'DEPÓSITO',
    };

    await mlab_utils.postEntity("movement", newMovement);

    let updatedAccount = {
      accountNumber: currentAccount.accountNumber,
      userId: currentAccount.userId,
      currency: currentAccount.currency,
      balance: currentAccount.balance + amount,
      openingDate: currentAccount.openingDate,
      status: currentAccount.status,
    };

    await mlab_utils.putEntity(
      "account",
      currentAccount._id.$oid,
      updatedAccount
    );

    response.status(200).send({msg: "Operación realizada"});

  } catch (err) {
    response.status(500).send({ msg: ERROR.GENERAL });
  }

});